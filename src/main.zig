const std = @import("std");
const c = @cImport({
    @cInclude("errno.h");
    @cInclude("unistd.h");
    @cInclude("syscall.h");
    @cInclude("sys/mman.h");
    @cInclude("wayland-client.h");
    @cInclude("wayland-cursor.h");
    @cInclude("xdg-shell.h");
    @cInclude("xkbcommon/xkbcommon.h");
    @cInclude("xkbcommon/xkbcommon-keysyms.h");
});

const TOTAL_BUFFERS: usize = 2;

const State = struct {
    keymap: [:0]const u8 = "us",
    width: usize = 800,
    height: usize = 600,
    stride: usize = undefined,
    display: ?*c.struct_wl_display = null,
    compositor: ?*c.struct_wl_compositor = null,
    shell: ?*c.struct_wl_shell = null,
    shm: ?*c.struct_wl_shm = null,
    xdg_shell: ?*c.zxdg_shell_v6 = null,
    seat: ?*c.struct_wl_seat = null,
    pointer: ?*c.struct_wl_pointer = null,
    keyboard: ?*c.struct_wl_keyboard = null,
    cursor_surface: ?*c.struct_wl_surface = null,
    cursor_image: ?*c.struct_wl_cursor_image = null,
    cursor_buffer: ?*c.struct_wl_buffer = null,
    surface: ?*c.struct_wl_surface = null,
    buffer: [TOTAL_BUFFERS]?*c.struct_wl_buffer = undefined,
    cur_buffer: usize = 0,
    buffer_data: ?*anyopaque = null,
    xkb_context: ?*c.xkb_context = null,
    xkb_state: ?*c.xkb_state = null,
    xkb_keymap: ?*c.xkb_keymap = null,
    quit: bool = false,

    pub fn getFreshPixelBuffer(self: *const @This()) []Pixel {
        const offset = self.height * self.width * self.cur_buffer;
        const pixels: []Pixel = @as([*]Pixel, @ptrCast(self.buffer_data.?))[offset .. offset + self.width * self.height];
        @memset(pixels, Pixel{ .red = 0, .green = 0, .blue = 0, .alpha = 0 });
        return pixels;
    }
};

// HANDLERS
//

fn registry_global_handler(
    data: ?*anyopaque,
    registry: ?*c.struct_wl_registry,
    name: u32,
    interface: [*c]const u8,
    version: u32,
) callconv(.C) void {
    const state: *State = @alignCast(@ptrCast(data));

    std.debug.print("interface: {s}, version: {}, name: {}\n", .{
        interface,
        version,
        name,
    });

    const len = std.mem.len(@as([*:0]const u8, @ptrCast(interface)));
    const interface_slice = interface[0..len];

    if (std.mem.eql(u8, interface_slice, "wl_compositor")) {
        state.compositor = @ptrCast(c.wl_registry_bind(registry, name, &c.wl_compositor_interface, 4));
    } else if (std.mem.eql(u8, interface_slice, "wl_shm")) {
        state.shm = @ptrCast(c.wl_registry_bind(registry, name, &c.wl_shm_interface, 1));
    } else if (std.mem.eql(u8, interface_slice, "wl_seat")) {
        state.seat = @ptrCast(c.wl_registry_bind(registry, name, &c.wl_seat_interface, 7));
        _ = c.wl_seat_add_listener(state.seat, &seat_listener, state);
    } else if (std.mem.eql(u8, interface_slice, "zxdg_shell_v6")) {
        state.xdg_shell = @ptrCast(c.wl_registry_bind(
            registry,
            name,
            &c.zxdg_shell_v6_interface,
            1,
        ));
    }
}

fn registry_global_remove_handler(
    data: ?*anyopaque,
    registry: ?*c.struct_wl_registry,
    name: u32,
) callconv(.C) void {
    _ = registry;
    _ = data;

    std.debug.print("removed name: {}\n", .{name});
}

const registry_listener = c.struct_wl_registry_listener{
    .global = registry_global_handler,
    .global_remove = registry_global_remove_handler,
};

fn xdg_toplevel_configure_handler(
    data: ?*anyopaque,
    xdg_toplevel: ?*c.zxdg_toplevel_v6,
    width: i32,
    height: i32,
    states: [*c]c.wl_array,
) callconv(.C) void {
    _ = states;
    _ = xdg_toplevel;
    _ = data;
    std.debug.print("configure: {}x{}\n", .{ width, height });
}

fn xdg_toplevel_close_handler(
    data: ?*anyopaque,
    xdg_toplevel: ?*c.zxdg_toplevel_v6,
) callconv(.C) void {
    _ = xdg_toplevel;
    _ = data;
    std.debug.print("close\n", .{});
}

const xdg_toplevel_listener = c.zxdg_toplevel_v6_listener{
    .configure = xdg_toplevel_configure_handler,
    .close = xdg_toplevel_close_handler,
};

fn xdg_surface_configure_handler(
    data: ?*anyopaque,
    xdg_surface: ?*c.zxdg_surface_v6,
    serial: u32,
) callconv(.C) void {
    _ = data;
    c.zxdg_surface_v6_ack_configure(xdg_surface, serial);
}

const xdg_surface_listener = c.zxdg_surface_v6_listener{
    .configure = xdg_surface_configure_handler,
};

fn xdg_shell_ping_handler(
    data: ?*anyopaque,
    this_xdg_shell: ?*c.zxdg_shell_v6,
    serial: u32,
) callconv(.C) void {
    _ = data;
    c.zxdg_shell_v6_pong(this_xdg_shell, serial);
    std.debug.print("ping-pong\n", .{});
}

const xdg_shell_listener = c.zxdg_shell_v6_listener{
    .ping = xdg_shell_ping_handler,
};

fn seat_capabilities_handler(
    data: ?*anyopaque,
    seat: ?*c.wl_seat,
    capabilities: u32,
) callconv(.C) void {
    _ = capabilities;
    _ = seat;
    _ = data;
    // TODO
}

fn seat_name_handler(
    data: ?*anyopaque,
    seat: ?*c.wl_seat,
    name: [*c]const u8,
) callconv(.C) void {
    _ = name;
    _ = seat;
    _ = data;
}

const seat_listener = c.wl_seat_listener{
    .capabilities = seat_capabilities_handler,
    .name = seat_name_handler,
};

fn pointer_enter_handler(
    data: ?*anyopaque,
    this_pointer: ?*c.wl_pointer,
    serial: u32,
    surf: ?*c.wl_surface,
    x: c.wl_fixed_t,
    y: c.wl_fixed_t,
) callconv(.C) void {
    _ = surf;
    _ = y;
    _ = x;
    const state: *State = @alignCast(@ptrCast(data));
    c.wl_pointer_set_cursor(
        this_pointer,
        serial,
        state.cursor_surface,
        @intCast(state.cursor_image.?.hotspot_x),
        @intCast(state.cursor_image.?.hotspot_y),
    );

    c.wl_surface_attach(state.cursor_surface, state.cursor_buffer, 0, 0);
    c.wl_surface_commit(state.cursor_surface);
}

fn pointer_leave_handler(
    data: ?*anyopaque,
    this_pointer: ?*c.wl_pointer,
    serial: u32,
    surf: ?*c.wl_surface,
) callconv(.C) void {
    _ = surf;
    _ = serial;
    _ = this_pointer;
    _ = data;
}

fn pointer_motion_handler(
    data: ?*anyopaque,
    this_pointer: ?*c.wl_pointer,
    time: u32,
    x: c.wl_fixed_t,
    y: c.wl_fixed_t,
) callconv(.C) void {
    _ = time;
    _ = y;
    _ = x;
    _ = this_pointer;
    _ = data;
}

fn pointer_button_handler(
    data: ?*anyopaque,
    this_pointer: ?*c.wl_pointer,
    serial: u32,
    time: u32,
    button: u32,
    state: u32,
) callconv(.C) void {
    _ = state;
    _ = button;
    _ = time;
    _ = serial;
    _ = this_pointer;
    _ = data;
}

fn pointer_axis_handler(
    data: ?*anyopaque,
    this_pointer: ?*c.wl_pointer,
    time: u32,
    axis: u32,
    value: c.wl_fixed_t,
) callconv(.C) void {
    _ = value;
    _ = axis;
    _ = time;
    _ = this_pointer;
    _ = data;
}

fn pointer_axis_source_handler(
    data: ?*anyopaque,
    pointer: ?*c.wl_pointer,
    axis_source: u32,
) callconv(.C) void {
    _ = axis_source;
    _ = pointer;
    _ = data;
}

fn pointer_axis_stop_handler(
    data: ?*anyopaque,
    pointer: ?*c.wl_pointer,
    time: u32,
    axis: u32,
) callconv(.C) void {
    _ = axis;
    _ = time;
    _ = pointer;
    _ = data;
}

fn pointer_axis_discrete_handler(
    data: ?*anyopaque,
    pointer: ?*c.wl_pointer,
    axis: u32,
    discrete: i32,
) callconv(.C) void {
    _ = discrete;
    _ = axis;
    _ = pointer;
    _ = data;
}

fn pointer_frame_handler(
    data: ?*anyopaque,
    pointer: ?*c.wl_pointer,
) callconv(.C) void {
    _ = pointer;
    _ = data;
}

const pointer_listener = c.struct_wl_pointer_listener{
    .enter = pointer_enter_handler,
    .leave = pointer_leave_handler,
    .motion = pointer_motion_handler,
    .button = pointer_button_handler,
    .axis = pointer_axis_handler,
    .axis_source = pointer_axis_source_handler,
    .axis_stop = pointer_axis_stop_handler,
    .axis_discrete = pointer_axis_discrete_handler,
    .frame = pointer_frame_handler,
};

fn keyboard_keymap_handler(
    data: ?*anyopaque,
    keyboard: ?*c.wl_keyboard,
    format: u32,
    fd: i32,
    size: u32,
) callconv(.C) void {
    _ = format;
    _ = keyboard;
    const state: *State = @alignCast(@ptrCast(data));
    // assert format == WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1

    std.debug.print("size: {}, fd: {}\n", .{ size, fd });

    const map_shm_ptr = c.mmap(
        null,
        size,
        c.PROT_READ,
        c.MAP_PRIVATE,
        fd,
        0,
    ).?;

    if (map_shm_ptr == c.MAP_FAILED.?) {
        std.debug.print("keymap: `mmap` failed! errno: {}\n", .{0});
        return;
    }

    const map_shm: [*]u8 = @ptrCast(map_shm_ptr);

    const xkb_keymap = c.xkb_keymap_new_from_string(
        state.xkb_context,
        map_shm,
        c.XKB_KEYMAP_FORMAT_TEXT_V1,
        c.XKB_KEYMAP_COMPILE_NO_FLAGS,
    );

    _ = c.munmap(map_shm, size);
    _ = c.close(fd);

    const xkb_state = c.xkb_state_new(xkb_keymap);
    c.xkb_keymap_unref(state.xkb_keymap);
    c.xkb_state_unref(state.xkb_state);
    state.xkb_keymap = xkb_keymap;
    state.xkb_state = xkb_state;
}

fn keyboard_enter_handler(
    data: ?*anyopaque,
    keyboard: ?*c.wl_keyboard,
    serial: u32,
    surface: ?*c.wl_surface,
    keys: ?*c.wl_array,
) callconv(.C) void {
    _ = keys;
    _ = surface;
    _ = serial;
    _ = keyboard;
    _ = data;
}

fn keyboard_leave_handler(
    data: ?*anyopaque,
    keyboard: ?*c.wl_keyboard,
    serial: u32,
    surface: ?*c.wl_surface,
) callconv(.C) void {
    _ = surface;
    _ = serial;
    _ = keyboard;
    _ = data;
}

fn keyboard_key_handler(
    data: ?*anyopaque,
    keyboard: ?*c.wl_keyboard,
    serial: u32,
    time: u32,
    key: u32,
    key_state: u32,
) callconv(.C) void {
    _ = time;
    _ = serial;
    _ = keyboard;
    const state: *State = @alignCast(@ptrCast(data));
    var buf: [128]u8 = .{0} ** 128;
    const keycode = key + 8;
    const sym = c.xkb_state_key_get_one_sym(state.xkb_state, keycode);
    _ = c.xkb_keysym_get_name(sym, buf[0..], buf.len);

    if (key_state == c.WL_KEYBOARD_KEY_STATE_PRESSED) {
        std.debug.print("Pressed {s}, sym: {}\n", .{ buf, sym });
    } else {
        std.debug.print("Released {s}, sym: {}\n", .{ buf, sym });
    }

    _ = c.xkb_state_key_get_utf8(state.xkb_state, keycode, buf[0..], buf.len);
    std.debug.print("utf8: {s}\n", .{buf});

    if (sym == c.XKB_KEY_Escape) {
        std.debug.print("user wants to leave!\n", .{});
        state.quit = true;
    }
}

fn keyboard_modifiers_handler(
    data: ?*anyopaque,
    keyboard: ?*c.wl_keyboard,
    serial: u32,
    mods_depressed: u32,
    mods_latched: u32,
    mods_locked: u32,
    group: u32,
) callconv(.C) void {
    _ = mods_latched;
    _ = group;
    _ = mods_locked;
    _ = mods_depressed;
    _ = serial;
    _ = keyboard;
    _ = data;
}

fn keyboard_repeat_info_handler(
    data: ?*anyopaque,
    keyboard: ?*c.wl_keyboard,
    rate: i32,
    delay: i32,
) callconv(.C) void {
    _ = delay;
    _ = rate;
    _ = keyboard;
    _ = data;
}

const keyboard_listener = c.struct_wl_keyboard_listener{
    .keymap = keyboard_keymap_handler,
    .enter = keyboard_enter_handler,
    .leave = keyboard_leave_handler,
    .key = keyboard_key_handler,
    .modifiers = keyboard_modifiers_handler,
    .repeat_info = keyboard_repeat_info_handler,
};

var t0: ?u32 = null;
var frames: usize = 0;

fn surface_frame_done(
    data: ?*anyopaque,
    cb: ?*c.wl_callback,
    time: u32,
) callconv(.C) void {
    const state: *State = @alignCast(@ptrCast(data));

    if (t0 == null) {
        t0 = time;
    }

    c.wl_callback_destroy(cb);

    // request new frame
    const new_cb = c.wl_surface_frame(state.surface);
    _ = c.wl_callback_add_listener(new_cb, &surface_frame_listener, state);

    // update image
    draw_frame(state, time);
    c.wl_surface_attach(state.surface, state.buffer[state.cur_buffer].?, 0, 0);
    c.wl_surface_damage(state.surface, 0, 0, std.math.maxInt(i32), std.math.maxInt(i32));
    c.wl_surface_commit(state.surface);
    frames += 1;

    state.cur_buffer = (state.cur_buffer + 1) % TOTAL_BUFFERS;

    if (t0 != null and time % 100 == 0) {
        const delta: f64 = @floatFromInt(time - t0.?);
        std.debug.print("frames: {}, time: {d:.0}, fps: {d:.0}\n", .{
            frames,
            delta,
            @as(f64, @floatFromInt(frames)) / (delta / 1000),
        });
    }
}

const surface_frame_listener = c.struct_wl_callback_listener{
    .done = surface_frame_done,
};

// end HANDLERS
//

const Pixel = struct {
    blue: u8,
    green: u8,
    red: u8,
    alpha: u8,
};

fn draw_frame(
    state: *State,
    time: u32,
) void {
    const pixels = state.getFreshPixelBuffer();
    const i = time / 1;

    for (0..state.width) |x| {
        for (0..state.height) |y| {
            var px = &pixels[y * state.width + x];

            if ((x + y + i) % 30 < 10) {
                px.alpha = 0;
            } else if ((x + y + i) % 30 < 20) {
                px.alpha = 255;
                px.red = 255;
                px.green = 255;
                px.blue = 0;
            } else {
                px.alpha = 64;
                px.red = 255;
                px.green = 0;
                px.blue = 0;
            }
        }
    }
}

pub fn main() u8 {
    var state = State{};
    state.display = c.wl_display_connect(0);

    if (state.display) |disp| {
        std.debug.print("address: {p}\n", .{disp});
        std.debug.print("connected!\n", .{});

        const registry = c.wl_display_get_registry(disp);
        _ = c.wl_registry_add_listener(registry, &registry_listener, &state);

        state.xkb_context = c.xkb_context_new(c.XKB_CONTEXT_NO_FLAGS);

        // wait for initial set of globals to appear
        _ = c.wl_display_roundtrip(disp);

        const cursor_theme = c.wl_cursor_theme_load(null, 100, state.shm).?;
        const cursor = c.wl_cursor_theme_get_cursor(cursor_theme, "cross").?; // left_ptr
        state.cursor_image = cursor.*.images[0];
        state.cursor_buffer = c.wl_cursor_image_get_buffer(state.cursor_image);
        state.cursor_surface = c.wl_compositor_create_surface(state.compositor).?;
        c.wl_surface_attach(state.cursor_surface, state.cursor_buffer, 0, 0);
        c.wl_surface_commit(state.cursor_surface);

        state.pointer = c.wl_seat_get_pointer(state.seat);
        _ = c.wl_pointer_add_listener(state.pointer, &pointer_listener, &state);

        state.keyboard = c.wl_seat_get_keyboard(state.seat);
        _ = c.wl_keyboard_add_listener(
            state.keyboard,
            &keyboard_listener,
            &state,
        );

        _ = c.zxdg_shell_v6_add_listener(
            state.xdg_shell,
            &xdg_shell_listener,
            &state,
        );

        if (state.compositor != null and state.xdg_shell != null and state.shm != null) {
            std.debug.print("got them\n", .{});
        } else {
            std.debug.print("did not get them\n", .{});
        }

        state.surface = c.wl_compositor_create_surface(state.compositor);
        const xdg_surface = c.zxdg_shell_v6_get_xdg_surface(state.xdg_shell, state.surface);
        _ = c.zxdg_surface_v6_add_listener(xdg_surface, &xdg_surface_listener, &state);
        const xdg_toplevel = c.zxdg_surface_v6_get_toplevel(xdg_surface);
        _ = c.zxdg_toplevel_v6_add_listener(xdg_toplevel, &xdg_toplevel_listener, &state);
        // c.zxdg_surface_v6_set_window_geometry(xdg_surface, 0, 0, 200, 200);
        c.zxdg_toplevel_v6_set_title(xdg_toplevel, "window title");

        // signal that the surface is ready to be configured
        c.wl_surface_commit(state.surface);

        const callback = c.wl_surface_frame(state.surface);
        _ = c.wl_callback_add_listener(callback, &surface_frame_listener, &state);

        state.stride = state.width * 4;
        const size = state.stride * state.height * TOTAL_BUFFERS;

        // open some anonymous file and write some 0 bytes to it
        const fd: c_int = @truncate(c.syscall(c.SYS_memfd_create, "buffer", @as(c_int, 0)));
        _ = c.ftruncate(fd, @intCast(size));

        // map it to the memory
        state.buffer_data = c.mmap(
            null,
            size,
            c.PROT_READ | c.PROT_WRITE,
            c.MAP_SHARED,
            fd,
            0,
        );

        // turn it into a shared memory pool
        const pool = c.wl_shm_create_pool(state.shm, fd, @intCast(size));

        // allocate N buffers in that pool
        for (0..TOTAL_BUFFERS) |i| {
            const offset = state.height * state.stride * i;
            state.buffer[i] = c.wl_shm_pool_create_buffer(
                pool,
                @intCast(offset),
                @intCast(state.width),
                @intCast(state.height),
                @intCast(state.stride),
                c.WL_SHM_FORMAT_ARGB8888,
            );
        }

        // wait for surface to be configured
        _ = c.wl_display_roundtrip(disp);

        c.wl_surface_attach(state.surface, state.buffer[state.cur_buffer], 0, 0);
        c.wl_surface_commit(state.surface);

        while (true and !state.quit) {
            _ = c.wl_display_dispatch(disp);
        }

        c.wl_display_disconnect(disp);
        return 0;
    } else {
        std.debug.print("failed to connect!\n", .{});
        return 1;
    }
}
